import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"rgba(234, 234, 255, 1)",
        paddingHorizontal:15,

    },
    inputMain:{
        width:"100%",
        // alignItems:"center",
        marginTop:30
    },
    eye:{
        width:30,
        height:30,
        alignItems:"center",
        justifyContent:"center",
        position:"absolute",
        top:145,
        right:19
    },
    eye1:{
        alignItems:"center",
        justifyContent:"center",
        width:30,
        height:30,
        position:"absolute",
        top:210,
        right:19
    },
    input:{
        width:"100%",
        height:54,
        backgroundColor:"rgba(40, 40, 40, 0.1)",
        paddingLeft:20,
        borderRadius:10,
        marginBottom:12
    },
    pressible:{
        width:"100%",
        height:51,
        backgroundColor:"#EEBA06",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:10,
        marginBottom:20,
        marginTop:20
    },
    qrCode:{
        width:"100%",
        height:450,
        backgroundColor:"white",
        marginTop:51,
        borderRadius:20,
        paddingHorizontal:28,
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    buttonView:{
        width:"100%",
        marginTop:23,
        justifyContent:"space-evenly",
        flexDirection:"row",
    },
    button:{
        justifyContent:"center",
        alignItems:"center",
        borderRadius:6
    },
    textButton:{
        fontSize:13,
        fontWeight:"bold",
    },
    underLine:{
        width:69,
        height:3,
        backgroundColor:"#EEBA06",
        position:"absolute",
        top:20,
        left:0
    },
    QrImage:{
        width:"100%",
        marginTop:48,
        justifyContent:"center",
        alignItems:"center"
    },
    textQr:{
        width:"100%",
        height:37,
        textAlign:"center",
        backgroundColor:"#CCE1E4",
        lineHeight:37,
        marginTop:18,
        borderRadius:10,
        justifyContent:"center",
        alignItems:"center"
    },
    errorBorder:{
        borderStyle:'solid',
        borderColor:"red",
        borderWidth:1,
    }
})