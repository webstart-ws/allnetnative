import React, { useState } from "react";
import { Image, Pressable, ScrollView,Text, TouchableWithoutFeedback, TextInput, TouchableWithoutFeedbackBase, View,BackHandler } from "react-native";
import { Lottery } from "../Lottery/Lottery";
import { styles } from "../register/registerStyle";
import { SvgComponent } from "../Svg";
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useEffect, useRef } from "react/cjs/react.development";
import { postRegister } from "../AllRequest/postRequest";


export function Register({navigation}) {
    BackHandler.addEventListener("hardwareBackPress", () => navigation.navigate("Home"));
    const username = useRef(""); const email = useRef("");const password = useRef("");const repeatePassword = useRef("");
    const [checkReg,setChecking] = useState(false)
    
    
    


    const  {Logo,Eye} = SvgComponent();
    const [colorButton,setButtonColor] = useState({
        oneButton:false,
        secondButton:false,
        securuty:true,
        securuty1:true
    });
    const [lazyLoad,setLazyLoad] = useState(true);

    let i = 0
    function onLoad(value) {
      i++
      if(i === 1) {
          setTimeout(() => {
              setLazyLoad(value)
          },500)
      }
    }
    console.log("register");





 

    return (
        <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <ScrollView style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]}>
            <Pressable style={styles.imgLogo}>
                <Logo />
            </Pressable>
            <View style={styles.qrCode}>
                <View style={styles.buttonView}>
                    <TouchableWithoutFeedback onPress={() => navigation.navigate("SignIn")}>
                        <Text style={[styles.textButton,{color:"#28282870"}]}>Sign in</Text>
                    </TouchableWithoutFeedback>
                    <Pressable style={styles.button} onPress={() => navigation.navigate("Register")}
                        onPressOut={() => setButtonColor({...colorButton,oneButton:false})} onPressIn={() => setButtonColor({...colorButton,oneButton:true})}
                    >
                        <Text style={[styles.textButton,{color:"#282828"}]}>Registration</Text>
                        <Text style={styles.underLine}></Text>
                    </Pressable>
                </View>

                <View style={styles.inputMain}>

                    {/* name */}
                    <TextInput 
                        style={[styles.input,checkReg ? styles.errorBorder : ""]}
                        onChangeText={(value) => username.current = value }
                        placeholder="Name"
                        onFocus={() => setChecking(false)}
                    />
                    {/* email */}
                    <TextInput 
                        style={[styles.input,checkReg ? styles.errorBorder : ""]}
                        onChangeText={(value) => email.current = value }
                        placeholder="E-Mail"
                        onFocus={() => setChecking(false)}
                    />

                    {/* password */}
                    <TextInput 
                        secureTextEntry={colorButton.securuty}
                        style={[styles.input,checkReg ? styles.errorBorder : ""]}
                        onChangeText={(value) => password.current = value }
                        placeholder="Password"
                        onFocus={() => setChecking(false)}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        setButtonColor({
                            ...colorButton,
                            securuty:!colorButton.securuty
                        })
                    }}>
                        <View style={styles.eye}>
                            <Eye/>
                        </View>
                    </TouchableWithoutFeedback>

                    {/* repeate password*/}
                    <TextInput 
                        secureTextEntry={colorButton.securuty1}
                        style={[styles.input,checkReg ? styles.errorBorder : ""]}
                        onChangeText={(value) => repeatePassword.current = value }
                        placeholder="Repeate Password"
                        onFocus={() => setChecking(false)}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        setButtonColor({
                            ...colorButton,
                            securuty1:!colorButton.securuty1
                        })
                    }}>
                        <View style={styles.eye1}>
                            <Eye/>
                        </View>
                    </TouchableWithoutFeedback>

                    <Pressable style={styles.pressible} onPress={() => {
                        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                        console.log(username.current);
                        console.log(email.current);
                        console.log(password.current);
                        console.log(repeatePassword.current);
                        if(
                            username.current !== "" && email.current && 
                            email.current.match(mailformat) && 
                            password.current.length > 5 &&  password.current === repeatePassword.current
                        ) {
                            postRegister({username:username.current,email:email.current,password:password.current})
                            .then((response) => {
                                console.log(response)
                                AsyncStorage.setItem("authenticated",response.access_token); 
                                navigation.navigate("MainPages")
                            })
                        } else {
                            setChecking(true)
                            setTimeout(function() { setChecking(false) },15000);
                        }
                        // username:input[0].value,email:input[1].value,password:input[2].value
                        // navigation.navigate("MainPages");
                    }}>
                   
                        <Text style={{fontSize:18,fontWeight:"bold"}}>Register</Text>
                    </Pressable>
                </View>



            </View>
        </ScrollView>
        </>
    )
}



{/* 
    <View style={styles.QrImage}>
        <Image source={require("../../assets/download.png")} 
            onLoadEnd={() => onLoad(true)}
        />
    </View> 
*/}

{/* 
    <View style={styles.textQr}>
        <Text style={{ fontSize:11,}}>Please for the registration use the QR code </Text>
    </View> 
*/}

{/* <TouchableWithoutFeedback>
    <Text style={{fontSize:11,color:"#28282870",textAlign:"center",marginTop:18}}>I agree to the terms ${"&"} conditions</Text>
</TouchableWithoutFeedback> */}