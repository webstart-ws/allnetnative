
import {Platform, StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)",
        // height:
    },
    imgLogo:{
        position:"relative",
        // marginTop:Platform.OS === "ios" ? 23 : 43,
        top:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    Text:{
        textAlign:"center",
        fontSize:30,
        fontWeight:"bold",
        width:"80%",
        marginTop:20
    },
    description:{
        textAlign:"center",
        fontSize:12,
        fontWeight:"bold",
        width:"90%",
        marginTop:15
    },
    buttonView:{
        width:"100%",
        marginTop:23,
        justifyContent:"space-evenly",
        flexDirection:"row",
        // paddingBottom:50
    },
    button:{
        width:154,
        height:42,
        backgroundColor:"#EEBA06",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:6
    },
    textButton:{
        fontSize:13,
        fontWeight:"bold",
    }
})