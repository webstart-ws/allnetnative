import React, { useEffect } from "react";
import { Image, Text, View, ScrollView, Pressable ,BackHandler} from "react-native";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { Colors } from "react-native/Libraries/NewAppScreen";
import { useRef, useState } from "react/cjs/react.development";
import { styles } from "../homePage/homeStyle";
import { Lottery } from "../Lottery/Lottery";
import { SvgComponent } from "../Svg";
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import { getHome } from "../AllRequest/getRequest";
import AsyncStorage from '@react-native-async-storage/async-storage'
import {en,am} from "../../localization"




export function Home({navigation}) {
    console.log("home");

    const { Logo } = SvgComponent()
    const [colorButton,setButtonColor] = useState({
        oneButton:false,
        secondButton:false,
    })
    const [lazyLoad,setLazyLoad] = useState(false)
    const [response,setResponse] = useState("")
    // const [description,setDescription] = useState("")
    function onLoad(value) {setTimeout(() => { setLazyLoad(value) },500)}
    BackHandler.addEventListener("hardwareBackPress", () => true);
    useEffect(() => {getHome().then((response) => {setResponse(response.home)})},[])
        
    return (
        <ScrollView>
            <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
            <ScrollView style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0,height:vh(100)}]}>
                <Pressable style={styles.imgLogo}>
                    <Logo />
                </Pressable>
                <View style={{marginTop:109,alignItems:"center"}}>
                    <Image source={require("../../assets/HomeImg.png")}
                        onLoadEnd={() => onLoad(true)}
                    />  
                    
                    {/* <Image style={{width:200,height:100}} source={{ uri:"https://www.artn.tv/assets/5ba5a4abe66dd.png"}}
                        onLoadEnd={() => onLoad(true)}
                    /> */}

                    <Text style={styles.Text}>
                        {response.title}
                    </Text>

                    <Text style={styles.description}>
                        {response.description}
                    </Text>

                </View>
                <View style={styles.buttonView}>
                    <TouchableWithoutFeedback 
                        onPress={() => navigation.navigate("SignIn")}
                        style={[styles.button,{backgroundColor:colorButton.oneButton ? "#5C58FF" : "#EEBA06"}]} 
                        onPressOut={() => setButtonColor({...colorButton,oneButton:false})} onPressIn={() => setButtonColor({...colorButton,oneButton:true})}
                    >
                        <Text style={[styles.textButton,{color:colorButton.oneButton ? "white" : "black"}]}>{en.home.signIn}</Text>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback 
                        style={[styles.button,{backgroundColor:colorButton.secondButton ? "#5C58FF" : "#EEBA06"}]} 
                        onPress={() => navigation.navigate("Register")}
                        onPressOut={() => setButtonColor({...colorButton,secondButton:false})} onPressIn={() => setButtonColor({...colorButton,secondButton:true})}
                    >
                        <Text style={[styles.textButton,{color:colorButton.secondButton ? "white" : "black"}]}>Registration</Text>
                    </TouchableWithoutFeedback>
                </View>
            </ScrollView>
        </ScrollView>
    )
}

// style={{flex:1,height:500}}
// {i18n.t('signIn')}

