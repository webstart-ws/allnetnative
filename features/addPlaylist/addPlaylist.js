import React, { useEffect, useState } from "react"
import { styles } from "../addPlaylist/addPlaylistStyle"
import { View,Image,Text,TouchableWithoutFeedback,TextInput,Pressable, Touchable,Touch, Keyboard, Dimensions  } from "react-native"
import { Menyu } from "../menyu/menyu"
import { ScrollView, TouchableHighlight } from "react-native-gesture-handler"
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { Lottery } from "../Lottery/Lottery"
import { SvgComponent } from "../Svg";


export function AddPlaylist({navigation}) {
  const  { 
    Logo, Smile, Beer, Poo, Bug, Glasses, Meh, PooSmall,
    BugSmall, GlassesSmall, MehSmall, BeerSmall, SmileSmall
  } = SvgComponent()


  const [color,setColor] = useState({
    backColor:[
      {key:0,color:"#00B624",activeColor:true},
      {key:1,color:"#4F4DFF",activeColor:false},
      {key:2,color:"#FF1F00",activeColor:false},
      {key:3,color:"#0085FF",activeColor:false},
      {key:4,color:"#FF607C",activeColor:false}
    ],
    icon:[
      {key:0,icon:SmileSmall,iconMainPage:Smile,activeColor:true},
      {key:1,icon:BeerSmall,iconMainPage:Beer,activeColor:false},
      {key:2,icon:BugSmall,iconMainPage:Bug,activeColor:false},
      {key:3,icon:GlassesSmall,iconMainPage:Glasses,activeColor:false},
      {key:4,icon:PooSmall,iconMainPage:Poo,activeColor:false},
      {key:5,icon:MehSmall,iconMainPage:Meh,activeColor:false}
    ],
    value:""
  })
  const [lazyLoad,setLazyLoad] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      setLazyLoad(true)
    },500)
  },[])

  
  return (
    <ScrollView>
      <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
      <TouchableWithoutFeedback onPress={() => {
        Keyboard.dismiss()
      }}>
        <View style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0,height:Platform.OS === "ios" ? useSafeAreaFrame().height : vh(100)}]}>
          <Pressable style={styles.imgLogo}>
              <Logo />
          </Pressable>
          <Text style={styles.contentText}>LOAD YOUR PLAYLIST</Text>
          
          <View style={{marginTop:25,paddingHorizontal:15,backgroundColor:"rgba(0, 0, 0, 0.02)",}}>
            <View style={styles.formik}>
              <TextInput
                style={styles.input}
                onChangeText={(value) => setColor({
                  ...color,
                  value:value
                })}
                placeholder="Playlist name"
                placeholderTextColor="#28282895"
              />
              <Text style={styles.inputText}>choose colors:</Text>

              <View style={styles.colorMain}>
                  {color.backColor.map((val,i) => {
                    return <Pressable key={val.key} style={[styles.color,{backgroundColor:val.color}]} onPress={() => {
                      setColor({
                        ...color,
                        backColor:color.backColor.map((val) => {
                          if(val.key === i) {return {...val,activeColor:true}} else {return { ...val,activeColor:false }}
                        })
                      })
                    }}>
                        {val.activeColor ? <View style={[styles.circleActive,{borderColor:val.color}]}></View> : null} 
                    </Pressable>
                  })}
              </View>

              <Text style={{fontSize:12,color:"#282828",fontWeight:"bold"}}>choose icons:</Text>

              <View style={styles.iconMAin}>
                  {color.icon.map((val,i) => {
                    return <Pressable key={val.key} style={[styles.iconSize,{ borderColor:val.activeColor ? "green" : "transparent",}]} onPress={() => {
                      setColor({
                        ...color,
                        icon:color.icon.map((val) => {
                          if(val.key === i) {return {...val,activeColor:true}} else {return { ...val,activeColor:false }}
                        })
                      })
                    }}>
                    {(function() {
                      const CustomTag = val.icon;
                      return <CustomTag/>
                    })()}

                      {/* {val.icon} */}
                    </Pressable>
                  })}
              </View>

              <Pressable style={styles.button} onPress={() => {
                navigation.navigate("MainPages",JSON.stringify(color))
              }}>
                <Text style={styles.text}>Add user</Text>
              </Pressable> 

            </View>
          </View>

          <Menyu navigation={navigation} backColor={"state3"} />
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  )
}