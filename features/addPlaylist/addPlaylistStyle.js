
import {Platform, StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)",
    },
    contentText:{
        marginTop:51,
        fontSize:25,
        fontWeight:"bold",
        paddingHorizontal:15
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15,
    },
    formik:{
        width:"100%",
        height:297,
        borderRadius:10,
        backgroundColor:"white",
        paddingHorizontal:25
    },
    input:{
        marginTop:28,
        paddingLeft: 16,
        width:"100%",
        height:38,
        borderRadius:5,
        opacity:0.9,
        backgroundColor:"rgba(0, 0, 0, 0.25)",
    },
    inputText:{
        marginTop:18,
        fontWeight:"bold",
    },
    colorMain:{
        width:145,
        height:20,
        justifyContent:"space-between",
        flexDirection:"row",
        marginVertical:14
    },
    color:{
        width:21,
        height:20,
        borderRadius:100,
        backgroundColor:"red",
        zIndex:1
    },
    iconMAin:{
        width:260,
        height:35,
        justifyContent:"space-between",
        flexDirection:"row",
        marginTop:14,
        marginBottom:18
    },
    iconSize:{
        width:35,
        height:35,
        borderRadius:10,
        justifyContent:"center",
        alignItems:"center",
        backgroundColor:"#rgba(0, 0, 0, 0.1)",
        shadowColor: "#rgba(0, 0, 0, 0.1)",
        borderWidth:2,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        height:41,
        borderRadius: 5,
        elevation: 32,
        backgroundColor: '#EEBA06',
    },
    text: {
        fontSize: 16,
        lineHeight: 21,
        fontWeight: 'bold',
        letterSpacing: 0.25,
        color: 'black',
    },
    circleActive:{
        position:"absolute",
        right:-5,
        top:-4,
        width:31,
        height:29,
        borderWidth:2,
        borderStyle:'solid',
        borderColor:"red",
        backgroundColor:"transparent",
        borderRadius:50
    }
})