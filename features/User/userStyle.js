
import {Platform, StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)",
    },
    title:{
        marginTop:44,
        flexDirection: 'row',
        alignItems: 'center',
        // justifyContent:"space-between",
    },
    Touchable:{
        width:38,
        height:38,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:"white",
        marginLeft:20,
        marginRight:32
    },
    titleText:{
        fontSize:25
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    upload:{
        width:"100%",
        paddingHorizontal:15,
        marginTop:28
    },
    containerUpload:{
        width:"100%",
        height:81,
        padding:14,
        backgroundColor:"#FFFFFF",
        justifyContent:"space-between",
        borderRadius:5,
        flexDirection:"row"
    },
    pressible:{
        width:"80%",
        height:55,
        backgroundColor:"#CCE1E4",
        borderRadius:5,
        justifyContent:"center",
        alignItems:"center"
    },

    InputMain:{
        width:"100%",
        height:45,
        flexDirection:"row",
        justifyContent:"space-between",
        paddingHorizontal:20,
        alignItems:"center",
        marginTop:13,
        backgroundColor:"#ffffff",
        borderRadius:5,
    },
    inputs:{
        width:100,
        borderStyle:"solid",
        borderWidth:1,
        borderColor:"black",
        borderRadius:50,
        paddingLeft:10,
    },
    Button:{
        width:"100%",
        height:51,
        backgroundColor:"#EEBA06",
        marginTop:15,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:10
    },
    languig:{
        width:"100%",
        height:45,
        flexDirection:"row",
        justifyContent:"space-between",
        paddingHorizontal:20,
        marginTop:13,
        backgroundColor:"#ffffff",
        borderRadius:5,
        alignItems:"center"
    },
    innerfLAG:{
        // width:"35%",
        flexDirection:"row",
        alignItems:"center",
        justifyContent:"space-between"
    }


})