import React, { useEffect, useState } from "react";
import { 
    Image, Pressable, Keyboard, Text, TextInput, TouchableWithoutFeedback, TouchableWithoutFeedbackBase, View, Button,
    BackHandler, ScrollView
} from "react-native";
import { Lottery } from "../Lottery/Lottery";
import { Menyu } from "../menyu/menyu";
import { styles } from "../User/userStyle";
// import * as ImagePicker from "react-native-image-picker"
import * as ImagePicker from 'expo-image-picker';
import { SvgComponent } from "../Svg";
import { getUserHomePage } from "../AllRequest/getRequest";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { changeUserData } from "../AllRequest/postRequest";



export function User({navigation,route}) {
    const  { Logo,Slack,Edit,AmFlag,EnFlag,LanguigSlack } = SvgComponent()

    const [textInputValue,setValue] = useState({
        name:"Name",
        nameLock:true,
        email:"E-Mail",
        emailLock:true,
        password:"",
        passwordLock:true
    })

    const [valueInput,setValueIInput] = useState({
        name:"",
        email:"",
        password:""
    })
    const [lazyLoad,setLazyLoad] = useState(false);
    const [image, setImage] = useState(null);

    useEffect(() => {
        setTimeout(() => {
            setLazyLoad(true)
        },500)
    },[])

    let hundleChooseFhoto = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });
        setImage(result.uri);
    }

    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", () => navigation.goBack());
        return () =>  {
            console.log("unmount Settings")
            BackHandler.addEventListener("hardwareBackPress", () => true);
        };
    }, []);

    // fetch data 
    useEffect(async () => {
        getUserHomePage(await AsyncStorage.getItem('authenticated'))
        .then(async (response) => {
            await AsyncStorage.setItem("userID",response.user.id + ""); 
            setValue({
                ...textInputValue,
                name:response.user.username,
                email:response.user.email,
                password:response.user.password,
            })
        })
    },[])
    console.log("Settings");


    return (
    <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <Pressable style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]} onPress={() => {
            Keyboard.dismiss()
        }}>
            <Pressable style={styles.imgLogo}>
                <Logo />
            </Pressable>

            <View style={styles.title}>
                <TouchableWithoutFeedback  onPress={() => navigation.goBack()}>
                    <View style={styles.Touchable}>  
                        <Slack/>
                    </View>
                </TouchableWithoutFeedback>
                <Text style={styles.titleText}>Settings</Text>
            </View>

            <ScrollView style={styles.upload}>
                {/* <View style={styles.containerUpload}>
                    <View>
                        <Image source={image ? {uri:image} : require("../../assets/userMain.png")}u
                            style={{ width: 55, height: 55, borderRadius:5}}
                        />
                    </View>
                    <Pressable style={styles.pressible} onPress={hundleChooseFhoto}> 
                        <Text>Uploud image</Text>
                    </Pressable>
                </View> */}

                <View style={[styles.InputMain,{borderColor:textInputValue.nameLock ? "white" : "black",}]}>
                    <Text>Name</Text>
                    <Text style={{display:textInputValue.nameLock ? "flex" : "none"}}>{textInputValue.name}</Text>
                    <TextInput 
                        style={[styles.inputs,{display:textInputValue.nameLock ? "none" : "flex",borderColor:textInputValue.nameLock ? "white" : "black",}]}
                        onChangeText={(value) => setValueIInput({
                            ...valueInput,
                            name:value,
                        })}
                        value={valueInput.name}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        if(valueInput.name !== ""){
                            setValue({
                                ...textInputValue,
                                nameLock:!textInputValue.nameLock,
                                name:valueInput.name,
                            })
                        } else {
                            setValue({
                                ...textInputValue,
                                nameLock:!textInputValue.nameLock,
                            })
                        }
                    }}>
                        <View style={{width:22,height:22, alignItems:"flex-end",justifyContent:"center"}}>
                            <Edit/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={styles.InputMain}>
                    <Text>E-mail</Text>
                    <Text style={{display:textInputValue.emailLock ? "flex" : "none"}}>{textInputValue.email}</Text>
                    <TextInput 
                        style={[styles.inputs,{display:textInputValue.emailLock ? "none" : "flex",borderColor:textInputValue.emailLock ? "white" : "black",}]}
                        onChangeText={(value) => setValueIInput({
                            ...valueInput,
                            email:value,
                        })}
                        value={valueInput.email}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                        if(valueInput.email.match(mailformat)){
                            setValue({
                                ...textInputValue,
                                emailLock:!textInputValue.emailLock,
                                email:valueInput.email,
                            })
                        } else {
                            setValue({
                                ...textInputValue,
                                emailLock:!textInputValue.emailLock,
                            })
                        }
                    }}>
                        <View style={{width:22,height:22, alignItems:"flex-end",justifyContent:"center"}}>
                            <Edit/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={styles.InputMain}>
                    <Text>Password</Text>
                    <TextInput
                        secureTextEntry={textInputValue.passwordLock ? true : false}
                        style={ [styles.inputs,{borderColor:textInputValue.passwordLock ? "white" : "black"}]  }
                        value={ textInputValue.password }

                        editable={true}
                        // editable={textInputValue.passwordLock ? false : true}
                        onChangeText={(value) => setValue({
                            ...textInputValue,
                            password:value,
                        })}
                        placeholder={textInputValue.password}
                    />
                    <TouchableWithoutFeedback onPress={() => {
                        setValue({
                            ...textInputValue,
                            passwordLock:!textInputValue.passwordLock,
                        })
                    }}>
                        <View style={{width:22,height:22, alignItems:"flex-end",justifyContent:"center"}}>
                            <Edit/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={[styles.languig]}>
                    <View style={styles.innerfLAG}>
                        <AmFlag/>
                        <Text style={{marginLeft:20}}>Armenian</Text>
                    </View>
                    <LanguigSlack/>
                </View>

                <View style={[styles.languig]}>
                    <View style={styles.innerfLAG}>
                        <EnFlag/>
                        <Text style={{marginLeft:20}}>English</Text>
                    </View>
                    <LanguigSlack/>
                </View>

                <Pressable style={styles.Button} onPress={async () => {
                    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    const userID = await AsyncStorage.getItem('userID')
                    if(textInputValue.name !== "" && textInputValue.email.match(mailformat) && textInputValue.password.length > 5) {
                        changeUserData({
                            token: await AsyncStorage.getItem('authenticated'),
                            body:{
                                username:textInputValue.nameLock ? textInputValue.name : valueInput.name !== "" ? valueInput.name : textInputValue.name ,
                                email:textInputValue.emailLock ? textInputValue.email : valueInput.email.match(mailformat) ? valueInput.email : textInputValue.email ,
                                password:textInputValue.passwordLock ? textInputValue.password : valueInput.password.length > 5 ? valueInput.password : textInputValue.password ,
                                id:+userID,
                            },
                            id:+userID
                        }).then((resp) => { console.log(resp) })
                    }
                }}>
                    <Text style={{fontSize:18,fontWeight:"bold"}}>Save</Text>
                </Pressable>
            </ScrollView>
          <Menyu navigation={navigation} backColor={"state2"}/>
        </Pressable>
    </>
    )
}

