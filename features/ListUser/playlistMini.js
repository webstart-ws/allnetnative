import React from "react"
import { View, Text, } from "react-native"
import {styles} from "../ListUser/listUserStyle"

export function PlaylistMini({route}) {
    return (
        <View style={styles.container}>
          <Text style={{marginTop:Platform.OS === "ios" ? 0 : 25}}>{route.params.text}</Text>
        </View>
    )
}