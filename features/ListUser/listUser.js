import React, {useEffect, useState} from "react"
import { styles } from "../ListUser/listUserStyle"
import { View,Image,Text,TouchableWithoutFeedback,Alert, Pressable } from "react-native"
import { Menyu } from "../menyu/menyu"
import { TouchableHighlight, TouchableOpacity } from "react-native-gesture-handler"
import { Lottery } from "../Lottery/Lottery"
import { SvgComponent } from "../Svg";



export function ListUser({navigation}) {
    const  { Logo, List, Dots } = SvgComponent()
    const [lazyLoad,setLazyLoad] = useState(false)


    const [playlistData,setPlaylistData] = useState([
      {key:0,imgRef:<List/>,color:"#EEBA06",text:"Playlist 1",secondText:"File: /Playlist / playlist.m3u"},
      {key:1,imgRef:<List/>,color:"#4F4DFF",text:"Playlist 2",secondText:"File: /Playlist / playlist.m3u"},
      {key:2,imgRef:<List/>,color:"#00B624",text:"Playlist 3",secondText:"File: /Playlist / playlist.m3u"}
    ]) 

    useEffect(() => {
      setTimeout(() => {
        setLazyLoad(true)
      },500)
    },[])

    return (
      <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <View style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]}>
          <Pressable style={styles.imgLogo}>
              <Logo/>
          </Pressable>

          <View style={styles.titleUserView}>
            <Text style={styles.contentText}>List User</Text>
            <TouchableWithoutFeedback onPress={() => Alert.alert("what is want to use","mainMessige",[
                {text:"yes", onPress:() => null},
                {text:"cancel",onPress:() => null},
            ])}>
              <View style={{width:30,height:20}}>
                <Dots/>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <View style={styles.playlistUser}>
            {playlistData.map((val,index) => {
              return  (
                <View key={val.key} style={{position:"relative"}}>
                  <TouchableOpacity style={styles.childPlaylistUser} onPress={() => navigation.navigate("PlaylistMini",val)}>
                    <View style={[styles.imgContent,{backgroundColor:val.color}]}>
                      {val.imgRef}
                    </View>

                    <View style={styles.textDiv}>
                      <View>
                        <Text style={{fontSize:16,fontWeight:"bold",marginBottom:5}}>{val.text}</Text>
                        <Text style={{fontSize:14,opacity:0.7}}>{val.secondText}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableWithoutFeedback  onPress={() => {
                    Alert.alert("Delete","Do you want to delete this playlist",[
                      {text:"Yes", onPress:() => {
                        setPlaylistData(playlistData.filter((val,i) => {
                          return i !== index
                        }))
                      }},
                      {text:"No",onPress:() => null},
                  ])}}>
                    <View style={styles.positionDots}>
                      <Image  source={require("../../assets/dots.png")}/>
                    </View>
                  </TouchableWithoutFeedback>
                </View>
            )})}
          </View>

          <Menyu navigation={navigation} backColor={"state4"}/>
        </View>
      </>
    )
}