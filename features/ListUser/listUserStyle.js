import {Platform, StyleSheet} from "react-native"


export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)",
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    contentText:{
        fontSize:25,
    },
    titleUserView:{
        marginTop:51,
        width:"100%",
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'flex-end',
        paddingHorizontal:15
    },
    playlistUser:{
        width:"100%",
        borderRadius:20,
        marginTop:25,
        paddingHorizontal:15
    },
    childPlaylistUser:{
        position:"relative",
        width:"100%",
        padding: 13,
        borderRadius: 20,
        flexDirection:"row",
        alignItems:"center",
        marginBottom:10,
        backgroundColor:"white",
    },
    imgContent:{
        backgroundColor:"#EEBA06",
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width:77,
        height:77
    },
    textDiv:{
        marginLeft:18,
    },
    positionDots:{
        alignItems:"center",
        width:36,
        height:21,
        position:"absolute",
        right:0,
        top:41,
        zIndex:5,
    }
})