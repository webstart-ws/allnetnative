import React, { useEffect, useState } from "react";
import { Image, Keyboard, Pressable, Text, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { Lottery } from "../Lottery/Lottery";
import { styles } from "./forgetPasswordStyle";


export function FindByEmail({navigation}) {

    const [lazyLoad,setLazyLoad] = useState(false)
    function onLoad(value) {
      setTimeout(() => {
        setLazyLoad(value)
      },500)
    }
    return (
        <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <Pressable style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]} 
            onPress={() => {Keyboard.dismiss()}}
        >
        <Image source={require("../../assets/Logo.png")}  style={styles.imgLogo} 
            onLoadEnd={() => onLoad(true)}
        />
        <View style={styles.FindByEmail}> 
            <Text style={{fontSize:18,color:"#111057",fontWeight:"bold",marginBottom:26}}>Forgot password ?</Text>
            <TextInput
                placeholder="Email"
                style={styles.Input}
            />
            <Pressable style={styles.pressible} onPress={() => navigation.navigate("CreatePassword")}> 
                <Text style={{fontSize:18,fontWeight:"bold"}}>
                    Continue
                </Text>
            </Pressable>
        </View>
        </Pressable>
    </>
    )
}
