import {Platform, StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)",
        paddingHorizontal:15
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
    },
    FindByEmail:{
        width:"100%",
        height:239,
        borderRadius:20,
        backgroundColor:"#FFFFFF",
        marginTop:111,
        paddingHorizontal:24,
        paddingTop:28
    },
    Input:{
        width:"100%",
        height:54,
        backgroundColor:"rgba(40, 40, 40, 0.1)",
        paddingLeft:20,
        borderRadius:10,
        marginBottom:12
    },
    pressible:{
        width:"100%",
        height:51,
        backgroundColor:"#EEBA06",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:10,
        marginBottom:20
    },
    eye:{
        position:"absolute",
        top:159,
        right:44
    },
    eye1:{
        position:"absolute",
        top:94,
        right:44
    }
})