import React, { useEffect, useState } from "react";
import { Image, Keyboard, Pressable, Text, TouchableWithoutFeedback, View } from "react-native";
import { TextInput } from "react-native-gesture-handler";
import { Lottery } from "../Lottery/Lottery";
import { styles } from "./forgetPasswordStyle";


export function CreatePassword({navigation}) {
    const [eye,setEye] = useState({
        eye1:true,
        eye2:true
    })

    const [lazyLoad,setLazyLoad] = useState(false)
    function onLoad(value) {
      setTimeout(() => {
        setLazyLoad(value)
      },500)
    }
    return (
        <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <Pressable style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]} 
            onPress={() => {Keyboard.dismiss()}}
        >
        <Image source={require("../../assets/Logo.png")}  style={styles.imgLogo}
            onLoadEnd={() => onLoad(true)}
        />
        <View style={[styles.FindByEmail,{height:315}]}> 
            <Text style={{fontSize:18,color:"#111057",fontWeight:"bold",marginBottom:26}}>New password</Text>
            <TextInput
                secureTextEntry={eye.eye1}
                placeholder="Password"
                style={styles.Input}
            />
            <TouchableWithoutFeedback onPress={() => {
                setEye({
                    ...eye,
                    eye1:!eye.eye1
                })
            }}>
                <View style={styles.eye1}>
                    <Image source={require("../../assets/eye.png")}/>
                </View>
            </TouchableWithoutFeedback >
            <TextInput
                secureTextEntry={eye.eye2}
                placeholder="Repeat Password"
                style={styles.Input}
            />
            <TouchableWithoutFeedback 
                onPress={() => {
                    setEye({
                        ...eye,
                        eye2:!eye.eye2
                    })
                }}>
                <View style={styles.eye}>
                    <Image source={require("../../assets/eye.png")}/>
                </View>
            </TouchableWithoutFeedback>
            <Pressable style={styles.pressible} onPress={() => navigation.navigate("MainPages")}> 
                <Text style={{fontSize:18,fontWeight:"bold"}}>
                    Continue
                </Text>
            </Pressable>
        </View>
        </Pressable>
    </>
    )
}