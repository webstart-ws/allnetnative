import React, { useEffect, useState } from "react";
import { Image, Keyboard, Linking, Pressable, Text, TextInput, TouchableNativeFeedback, TouchableWithoutFeedback, View,BackHandler } from "react-native";
import { postSignIn } from "../AllRequest/postRequest";
import { Lottery } from "../Lottery/Lottery";
import { styles } from "../signIn/signInStyle";
import { SvgComponent } from "../Svg";
import AsyncStorage from '@react-native-async-storage/async-storage'


export function SignIn({navigation}) {
    const [lazyLoad,setLazyLoad] = useState(false)
    const  {Logo, Eye} = SvgComponent()

    const [colorButton,setButtonColor] = useState({
        oneButton:false,
        secondButton:false,
        securuty:true
    })
    const [authentication,setAuth] = useState(true)
    const [inputValue,setInputValue] = useState({
        email:"",
        password:""
    })

    useEffect(() => { 
        setTimeout(() => { 
            setLazyLoad(true)   
        },500) 
    },[])
    BackHandler.addEventListener("hardwareBackPress", () => navigation.navigate("Home"));


    console.log("login");




    return (
        <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
            <Pressable  style={[styles.container,{zIndex:0,opacity:lazyLoad ?  1 : 0}]} onPress={() => {
                Keyboard.dismiss()
            }}>
                <Pressable style={styles.imgLogo}>
                    <Logo />
                </Pressable>
                <View style={styles.SignInView}>
                    <View style={styles.SignInViewchild}>
                        <View style={styles.buttonView}>
                            <TouchableWithoutFeedback onPress={() => navigation.navigate("SignIn")}>
                                <Text style={[styles.textButton,{color:"black"}]}>Sign in</Text>
                            </TouchableWithoutFeedback>
                            <TouchableWithoutFeedback style={styles.button} onPress={() => navigation.navigate("Register")}>
                                <Text style={[styles.textButton,{color:"#28282870"}]}>Registration</Text>
                            </TouchableWithoutFeedback>
                            <Text style={styles.underLine}></Text>
                        </View>

                        <View style={styles.inputMain}>
                            {/* name */}
                            <TextInput 
                                value={inputValue.email}
                                style={[styles.input,{borderColor:authentication ? "transparent" : '#FF3D00',borderWidth: 2,}]}
                                onChangeText={(value) => {setInputValue({...inputValue,email:value})}}
                                placeholder="E-Mail"
                                onFocus={() => setAuth(true)}
                            />
                            {/* email */}
                            <TextInput 
                                value={inputValue.password}
                                secureTextEntry={colorButton.securuty}
                                style={[styles.input,{borderColor:authentication ? "transparent" : '#FF3D00',borderWidth: 2,}]}
                                onChangeText={(value) => {setInputValue({...inputValue,password:value})}}
                                placeholder="Password"
                                onFocus={() => setAuth(true)}
                            />
                            <TouchableWithoutFeedback onPress={() => {
                                setButtonColor({
                                    ...colorButton,
                                    securuty:!colorButton.securuty
                                })
                            }}>
                                <View style={styles.eye}>
                                    <Eye/>
                                </View>
                            </TouchableWithoutFeedback>
                            <View style={[styles.textUnderLine,{alignSelf:"flex-end"}]}>
                                <TouchableWithoutFeedback onPress={() => navigation.navigate("FindByEmail")}>
                                    <Text>Forgot your password ?</Text>
                                </TouchableWithoutFeedback>
                            </View>

                            <Pressable style={styles.pressible} onPress={() => {
                                const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                                if(inputValue["email"].match(mailformat) && inputValue["password"].length > 5) {
                                    postSignIn({body:{email:inputValue["email"],password:inputValue["password"]}})
                                    .then(async (response) => {
                                        if( !!response["error"]) {setAuth(false);setTimeout(() => {setAuth(true)},3000) }
                                        else { 
                                            AsyncStorage.setItem("authenticated",response.access_token); 
                                            navigation.navigate("MainPages")
                                        }
                                    })
                                } 
                                else { setAuth(false);setTimeout(() => {setAuth(true)},15000) }
                            }}>
                                <Text style={{fontSize:18,fontWeight:"bold"}}>Sign In</Text>
                            </Pressable>
                        </View>

                    </View>
                </View>
            </Pressable>
        </>
    )
}






// social Register 
{
    /* <View style={styles.buttonUnderLine}>
        <Text style={{fontSize:12,color:"#28282870"}}>Registration via social networks</Text>
    </View> */
}

{
/* <View style={styles.locationButton}>
    <TouchableWithoutFeedback onPress={() => Linking.openURL('https://www.facebook.com/')}>
        <View style={styles.location}>
            <FaceBook />
        </View>
    </TouchableWithoutFeedback>

    <TouchableWithoutFeedback onPress={() => Linking.openURL('https://www.google.ru/')} >
        <View style={[styles.location,{backgroundColor:"#FF1F00"}]}>
        <Google/>
        </View>
    </TouchableWithoutFeedback>

    <TouchableWithoutFeedback onPress={() => Linking.openURL('https://twitter.com/Twitter?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor')}>
        <View style={[styles.location,{backgroundColor:"#2A29B4"}]}>
            <Tvitter/>
        </View>
    </TouchableWithoutFeedback>

    <TouchableWithoutFeedback onPress={() => Linking.openURL('https://web.telegram.org/k/')}>
        <View style={[styles.location,{backgroundColor:"#0085FF"}]}>
            <Telegram/>
        </View>
    </TouchableWithoutFeedback>
</View> */
}