import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    containerMulty:{
        flex:1,
        backgroundColor: '#E5E5E5'
    },
    goBack:{
        width:38,
        height:38,
        borderRadius:10,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:"white"
    },
    arrow:{
        width:"100%",
        paddingHorizontal:38,
        flexDirection:"row",
        alignItems:"center",
    },
    kubicMain:{
        justifyContent:"center",
        alignItems:"center",
        borderRadius:10
    },
    childKubic:{
        flexDirection:"row",
        flexWrap:"wrap",
        justifyContent:"space-between",
        alignContent:"space-between"
    },
    oneCubic:{
        width:"49%",
        height:"48%",
        backgroundColor:"#4F4DFF"
    }
})
