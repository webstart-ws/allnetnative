import React, { useEffect, useState } from 'react';
import { Dimensions, Image, Text, View } from 'react-native';
import { TouchableWithoutFeedback,Platform } from 'react-native';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
// import { Orientation } from '../screenInfo/screenInfo';
import {styles} from "./multiScreenStyle"
import Orientation from 'react-native-orientation'
import { Lottery } from '../Lottery/Lottery';
import { SvgComponent } from "../Svg";




export function MultyScreen({navigation}) {

    const [Lock,setLock] = useState(true)
    const [lazyLoad,setLazyLoad] = useState(false)
    const  {Slack} = SvgComponent()
 
    useEffect(() => {
        Dimensions.addEventListener("change",() => setLock(!Lock))
    },[Lock])

    useEffect(() => {
        setTimeout(() => {
            setLazyLoad(true)
        },500)
    },[])


 

    return (
    <>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <ScrollView style={[styles.containerMulty,{zIndex:0,opacity:lazyLoad ?  1 : 0}]}>
            <View style={[styles.arrow,{paddingTop: Platform.OS === "ios" ? 22 : 44}]}>
                <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
                    <View style={styles.goBack}>
                        <Slack/>
                    </View>
                </TouchableWithoutFeedback>
                <Text style={{marginLeft:29,fontWeight:"bold"}}>Multy-Screen</Text>
            </View>
            <View style={{flex:1,justifyContent:"space-evenly",flexDirection:"row", flexWrap:'wrap'}}>
                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={styles.oneCubic}></View>
                        <View style={styles.oneCubic}></View>
                        <View style={styles.oneCubic}></View>
                        <View style={styles.oneCubic}></View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={[styles.oneCubic,{width:"100%"}]}></View>
                        <View style={{width:"32%",height:"48%",backgroundColor:"#4F4DFF"}}></View>
                        <View style={{width:"33%",height:"48%",backgroundColor:"#4F4DFF"}}></View>
                        <View style={{width:"33%",height:"48%",backgroundColor:"#4F4DFF"}}></View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={[styles.oneCubic,{width:"100%"}]}></View>
                        <View style={{width:"49%",height:"48%",backgroundColor:"#4F4DFF"}}></View>
                        <View style={{width:"49%",height:"48%",backgroundColor:"#4F4DFF"}}></View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={styles.oneCubic}></View>
                        <View style={styles.oneCubic}></View>
                        <View style={[styles.oneCubic,{width:"100%"}]}></View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={[styles.oneCubic,{height:"100%"}]}></View>
                        <View style={[styles.oneCubic,{height:"100%"}]}></View>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={[styles.kubicMain,{width:Lock ? "100%" : 217,height:124,backgroundColor:"#CECDFF",marginTop:16,marginBottom:50}]}>
                    <View style={[styles.childKubic,{width:"80%",height:76}]}>
                        <View style={[styles.oneCubic,{height:"100%"}]}></View>
                        <View style={[styles.oneCubic,{height:"100%"}]}></View>
                    </View>
                </TouchableOpacity>
            </View>
        </ScrollView>
    </>

    )
}