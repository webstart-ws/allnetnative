import React, { useState ,useEffect} from 'react';
import {styles} from "./menyuStyle"
import { View,Image ,TouchableWithoutFeedback,Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { SvgComponent } from "../Svg";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { postLogAuth } from '../AllRequest/postRequest';


export function Menyu({navigation,backColor}) {   
    const  {
        Plus, UserBlack, UserWhite, HomeBlack, 
        HomeWhite, CogBlack, CogWhite, BellBlack ,
        BellWhite, SlackMenyuGreen, SlackMenyuBlue
    } = SvgComponent()
    // {backColor === "state1" ? <View style={[styles.slack,{ left:4}]}></View> : null}
    // {backColor === "state1" ? <View style={[styles.slack,{left:7,bottom:-21}]}></View> : null}


  return (
    <>
        <View style={styles.Menyu}>
            <View style={styles.miniMenyu}>
                <TouchableWithoutFeedback onPress={() => {
                        navigation.navigate("MainPages")
                    }}>
                    <View style={[styles.Touchable,backColor === "state1" ? styles.TouchableActive : null]}>
                            {backColor === "state1" ? <HomeWhite/> : <HomeBlack/> }
                            {backColor === "state1" ? <View style={[styles.slack,{ left:4}]}><SlackMenyuGreen/></View> : null} 
                    </View>
                </TouchableWithoutFeedback>


                <TouchableWithoutFeedback onPress={() => {
                    navigation.navigate("User")
                }}>
                    <View style={[styles.Touchable,backColor === "state2" ? styles.TouchableActive : null]}>
                        {backColor === "state2" ? <CogWhite/> : <CogBlack/> }
                        {backColor === "state2" ? <View style={[styles.slack,{ left:4}]}><SlackMenyuGreen/></View> : null}
                    </View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={() => {
                    navigation.navigate("LiveTv")
                }}>
                    <View style={styles.TouchablePlus}>
                        <Plus/>
                        {backColor === "state3" ? <View style={[styles.slack,{left:7,bottom:-21}]}><SlackMenyuBlue/></View> : null}
                    </View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback  onPress={() => {
                    navigation.navigate("Notification")
                }}>
                    <View style={[styles.Touchable,backColor === "state5" ? styles.TouchableActive : null]}>
                        {backColor === "state5" ? <BellWhite/> : <BellBlack/> }
                        {backColor === "state5" ? <View style={[styles.slack,{ left:4}]}><SlackMenyuGreen/></View> : null}
                    </View>
                </TouchableWithoutFeedback>
            
                <TouchableWithoutFeedback onPress={() => Alert.alert("do you want exit page ? ","Confirm",[
                    {text:"yes", onPress:async () => {
                        postLogAuth(await AsyncStorage.getItem('authenticated'),navigation)
                    }},
                    {text:"cancel",onPress:() => null},
                ])}>
                    <View style={[styles.Touchable,backColor === "state4" ? styles.TouchableActive : null]}>
                        <UserBlack/>
                        {/* {backColor === "state4" ?  <UserWhite/>  : <UserBlack/>} */}
                        {/* {backColor === "state4" ? <View style={[styles.slack,{ left:4}]}><SlackMenyuGreen/></View> : null} */}
                    </View>
                </TouchableWithoutFeedback>
            </View>
        </View>
    </>
  );
};


