import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    Menyu:{
        height:76,
        width:"100%",
        backgroundColor: '#FFFFFF',
        position: "absolute",
        bottom:0,
        alignItems: 'center',
      },
      miniMenyu:{
        marginTop:10,
        width:"80%",
        flexDirection: 'row',
        justifyContent:"space-between",
        alignItems: 'center',
      },
      Touchable:{
        position:"relative",
        width:38,
        height:38,
        justifyContent:"center",
        alignItems: 'center',
        borderRadius:50,
      },
      TouchableActive:{
        backgroundColor:"#00B624",
      },
      TouchablePlus:{
        position:"relative",
        width:45,
        height:45,
        backgroundColor:"rgba(79, 77, 255, 1)",
        justifyContent:"center",
        alignItems: 'center',
        borderRadius:50
      },
      slack:{
        position: 'absolute',
        bottom:-25,
      }
})
