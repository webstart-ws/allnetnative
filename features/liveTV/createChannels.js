

export function createChannels(response,state,setState) {
    try {
        const allChannels = response.tariffType[0].bouquet_id 
        const concatChannel = allChannels.reduce((aggr,val,i,array) =>  {
            aggr = aggr.concat(val.bouquet_channels)
            return aggr
        },[])
        const AllType = [concatChannel,...allChannels.map((val) => val.bouquet_channels)]
        setState({ ...state,fuutbollChannel:AllType })
    }
    catch {
        setState({ ...state,fuutbollChannel:[null]})
    }
}


// "https://www.artn.tv/assets/5ba5a4abe66dd.png"