import React, { useEffect, useState } from "react"
import { View,Image,TouchableWithoutFeedback,Text,
    TouchableOpacity,FlatList,TextInput,ScrollView, Keyboard, Dimensions, Button, Pressable, BackHandler
} from "react-native"
import {styles} from "./styleLiveTv"
import { Menyu } from "../menyu/menyu"
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { Lottery } from "../Lottery/Lottery";
import { SvgComponent } from "../Svg";
import { getUserHomePage } from "../AllRequest/getRequest";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { createChannels } from "./createChannels";




export function LiveTv({navigation}) {
    console.log("live tv");
    const  {Logo, Slack, Search, Async} = SvgComponent()


    // state
    const [state,setState] = useState({
        fuutbollChannel:[ /**[{key:1,stream_icon:"",stream_display_name:"1 HD"}]*/ ],

        pagesData:[
            {key:1,text:"All",active:true},
            {key:2,text:"Favorites",active:false},
            {key:3,text:"Channel",active:false}
        ]
    })

    // console.log(state);
    var refIndex = state.pagesData.findIndex((val) => val.active )

    // lazy load
    const [lazyLoad,setLazyLoad] = useState(true)
    let r = 0
    function onLoad() {
        r += 1
        if(state.fuutbollChannel.length === r) {
            setTimeout(() => { setLazyLoad(true) },500)
        }
    }
    
    // BackHandler function
    useEffect(() => {
        console.log("live tv mount");
        BackHandler.addEventListener("hardwareBackPress", () => navigation.goBack());
        return () =>  {
            console.log("unmount live tv")
            BackHandler.addEventListener("hardwareBackPress", () => true);
        };
    }, []);

    // fetch Data 
    useEffect(async () => {
        console.log("request");
        getUserHomePage(await AsyncStorage.getItem('authenticated'))
        .then((response) => {
            createChannels(response,state,setState)
        })
    },[])

    return (
    <ScrollView>
        <TouchableWithoutFeedback onPress={() => {Keyboard.dismiss()}} >
            <View style={[styles.container,{ zIndex:0,opacity:lazyLoad ?  1 : 0,height:Platform.OS === "ios" ? useSafeAreaFrame().height : vh(100)}]} >
                <Pressable style={styles.imgLogo}>
                    <Logo />
                </Pressable>
                <View style={[styles.title]}>
                    <View style={styles.title}>
                        <TouchableWithoutFeedback  onPress={() => navigation.goBack()}>
                            <View style={styles.Touchable}>
                                <Slack/>
                            </View>
                        </TouchableWithoutFeedback>
                        <Text style={styles.titleText}>Live tv</Text>
                    </View> 
                    <TouchableWithoutFeedback onPress={() => alert("refreshg")}>
                        <View style={[styles.buttonRefresh,{marginRight:15,alignSelf:"flex-end"}]}>
                            <Async/>
                        </View>
                    </TouchableWithoutFeedback>
                </View>

                <View style={styles.form}>
                    <View style={styles.imgInput}>
                        <Search/>
                    </View>
                    <TextInput 
                        style={styles.input}
                        onChangeText={(value) => console.log(value)}
                        placeholder="Search.."
                    />
                </View>

                <View style={styles.mainFlat}>
                    <FlatList  horizontal data={state.pagesData} renderItem={({item}) => {
                        return <TouchableOpacity onPress={() => {
                            setState({
                                ...state,
                                pagesData:state.pagesData.map((val) => {
                                    if(item.key === val.key) {
                                        return {...val,active:true } 
                                    } else {
                                        return {...val,active:false}
                                    }
                                })
                            })
                        }} style={[styles.MovePage,
                            {
                                width:item.key === 1 ? 113 : item.key === 2 ? 158 : item.key === 3 ? 158 : "initial",
                                backgroundColor:item.active === true ?  "#4F4DFF" : "white",
                            }
                        ]}>
                            <Text style={{color:item.active === true ?  "white" : "black",fontWeight:"600",fontSize:18}}>{item.text}</Text>
                        </TouchableOpacity>
                    }} 
                    showsHorizontalScrollIndicator={false}/>
                </View>

                <ScrollView showsVerticalScrollIndicator={false} style={styles.mainBlock}>
                    <View style={styles.innerScroll}>
                            {
                                Array.isArray(state.fuutbollChannel[0]) ? 
                                state.fuutbollChannel[refIndex].map((item,i) => {
                                    return <TouchableOpacity key={i} style={styles.vision} onPress={() => navigation.navigate("Channel",item)}>
                                        <Image style={{width:70,height:30, resizeMode: "contain", marginTop:25,}} source={  !!item.stream_icon ?  { uri: item.stream_icon  } : require(`../../assets/footbolHdImg.png`)}  onLoadEnd={() => { onLoad() }}/> 
                                        <Text style={{marginTop:12,fontSize:14, textAlign: "center"}}>{item.stream_display_name || ""}</Text>
                                    </TouchableOpacity>
                                }) : 
                                state.fuutbollChannel[0] === null ? 
                                <Text style={styles.doNotData}>There are'nt channels</Text> :
                                <Image style={ { flex: 2 }} source={ require(`../../assets/homeLoading.gif`)} /> 
                            }
                    </View>
                </ScrollView>
                <Menyu navigation={navigation} backColor={"state3"}/>
            </View>
        </TouchableWithoutFeedback>
    </ScrollView>
    )
}



//  <Image source={  !!item.stream_icon ?  { uri: item.stream_icon  } : require(`../../assets/footbolHdImg.png`)}  onLoadEnd={() => { onLoad() }}/> 
{/* <Image source={  { uri: item.stream_icon  } }  onLoadEnd={() => { onLoad() }}/>  */}


{/* <FlatList data={data} renderItem={({item}) => (   
)} style={styles.Data} numColumns={3}/> */}

// require(`../../assets/footbolHdImg.png`)
// style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}