import {Platform, StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:"rgba(234, 234, 255, 1)"
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    Touchable:{
        width:38,
        height:38,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:"white",
        marginLeft:20,
        marginRight:32
    },
    title:{
        marginTop:10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:"space-between",
        // backgroundColor:"red"
    },
    titleText:{
        fontSize:25
    },
    Data:{
        flex:1,
        flexDirection: 'row',
        marginTop:25,
    },
    vision:{
        width:105,
        height:105,
        borderRadius:20,
        backgroundColor:"white",
        // justifyContent: 'center',
        alignItems:"center",
        marginBottom:15
    },
    mainBlock:{
        flex: 1,
        marginTop:27.5,
    },
    innerScroll:{
        flex: 1, 
        marginBottom: 100,  
        flexWrap: 'wrap', 
        flexDirection: 'row', 
        justifyContent: 'space-between',
        marginHorizontal:15
    },
    form:{
        width:"100%",
        position: "relative",
        marginTop: 28,
        marginLeft: 15,
    },
    input:{
        paddingLeft: 56,
        width:342,
        height:40,
        borderRadius:50,
        backgroundColor:"white"
    },
    imgInput:{
        position: "absolute",
        left:23,
        top:12,
        zIndex:1
    },
    MovePage:{
        paddingVertical: 11,
        paddingHorizontal: 27,
        borderRadius: 10,
        backgroundColor: "white",
        justifyContent: 'center',
        alignItems: 'center',
        marginRight:15
    },
    pagesData:{
        height:42
    },
    mainFlat:{
        marginTop:29,
        marginLeft:15,
    },
    buttonRefresh:{
        width:48,
        height:48,
        backgroundColor:"#00B624",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:50
    },
    content:{
        width:"100%",
        height:10
    },
    doNotData:{
        flex:1,
        fontSize:50,
        color:"red",
        textAlign:"center",
        justifyContent:"center",
        marginTop:30
    }
})