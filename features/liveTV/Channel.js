import React from "react"
import { View,Image,TouchableWithoutFeedback,Text,
    TouchableOpacity,FlatList, Platform
} from "react-native"
import video from '../../assets/saro.mp4';
import { Video } from "expo-av";
import {styles} from "./styleLiveTv"

export function Channel({route}) {
    console.log(route.params.stream_source[0]);
    return (
        <View style={styles.container}>
                <Video  
                    source={ { uri: route.params.stream_source[0]} }
                    // source={video}                  // the video file
                    style={{width:300,height:500,backgroundColor:"red"}}  // any style you want
                    resizeMode="contain"   
                    // isLooping
                    useNativeControls     
                    // onPlaybackStatusUpdate={(event) => console.log(event)}   
                    volume={1}    
                />
            <Text style={{paddingRight:Platform.OS === "ios" ? 0 : 25}}>{"route"}</Text>
        </View>
    )
}

// "http://fcf2e861.ucomist.net/iptv/CB5F2GMTR7SUDF/11007/index.m3u8"
