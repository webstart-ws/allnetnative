import React from "react";
import {View,Text, Platform}from "react-native";
import { styles } from "./mainPageStyle";

export function Playlist({route}) {
    let PareseParam = JSON.parse(route.params)

    return (
        <View>
            <Text style={{marginTop:Platform.OS === "ios" ? 0 : 25}}>{PareseParam.text}</Text>
        </View>
    )
}