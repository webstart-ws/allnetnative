import  {StyleSheet, Platform} from "react-native"

export const styles = StyleSheet.create({

  container: {
    position:"relative",
    flex: 1,
    backgroundColor: '#6E6EF4',
  },
  centerModeText:{
    textAlign:"center",
    paddingTop: 56,
    fontSize:30,
    paddingHorizontal:100,
    fontWeight:"800",
    color:"rgba(40, 40, 40, 1)",
    fontWeight:"bold"
  },
  view2: {
    backgroundColor: 'rgba(206, 205, 255, 1)',
    width:"100%",
    height:346
  },
  innerView3:{
    backgroundColor: '#EAEAFF',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    width:"100%",
    height:746,
  },
  backgroundBottom:{
    position: 'absolute',
    bottom:0,
    left:0,
    width:"100%",
    height:55,
    zIndex:0,
    backgroundColor: '#6E6EF4'
  },
  text:{
    marginTop:0,
    color:"red",
    fontSize:120

  },
  button:{
    backgroundColor:"blue"
  },
  imgLogo:{
    marginTop:Platform.OS === "ios" ? 23 : 43,
    marginLeft:15,
  },
  mainPageImg:{
    marginTop:Platform.OS === "ios" ? 79 : 59,
    justifyContent:"center",
    zIndex:1,
    width:"100%",
    flexDirection:"row"
  },
  form:{
    width:"100%",
    position: "relative",
    marginTop: 27,
    flexDirection: 'row',
    marginLeft:15
  },
  input:{
    paddingLeft: 56,
    width:342,
    height:40,
    borderRadius:50,
    backgroundColor:"white",
    paddingTop:0
    // paddingTop:Platform.OS === "ios" ? 12 : 0
  },
  imgInput:{
    position: "absolute",
    left:20,
    top:12,
    zIndex:1
  },
  slider:{
    marginTop:38,
    marginBottom: 38,
    marginLeft: 15,
  },
  childSlider:{
    width:149,
    height:149,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 27,
    marginRight: 15,
  },
  textSlider:{
    fontSize:18,
    fontWeight:"bold",
    marginTop: 17,
    color:"white"
  },
  multiPlay:{
    flex:1,
    alignItems:"center"
  },
  multy:{
    width:340,
    height:64,
    backgroundColor: "white",
    marginBottom:16,
    paddingLeft: 10,
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 10,
  },
  miniMulty:{
    width:46,
    height:46,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#2A29B4",
    borderRadius: 5,
  },
});
  