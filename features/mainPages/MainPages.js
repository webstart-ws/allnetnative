import React, { useState } from 'react';
import {styles} from "./mainPageStyle"
import { Text, View,Image,TextInput, Platform, StatusBar ,
  FlatList, TouchableOpacity, ScrollView, TouchableHighlight, KeyboardAvoidingView ,Dimensions, Pressable, Button, BackHandler, Alert
} from 'react-native';
import { Menyu } from '../menyu/menyu';
import { useEffect } from 'react/cjs/react.development';
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import { useWindowDimensions } from 'react-native';
import { useSafeAreaFrame } from 'react-native-safe-area-context';
import { Lottery } from '../Lottery/Lottery';
import { SvgComponent } from "../Svg";
import {Formik} from "formik"
import { useRoute } from '@react-navigation/native';
import { getUserHomePage } from '../AllRequest/getRequest';
import AsyncStorage from '@react-native-async-storage/async-storage'
import { postLogAuth } from '../AllRequest/postRequest';



// import WebView from 'react-native-webview';


export function MainPages({navigation,route}) {   
  const  { Logo, Search, CogWhite, Kubic, Smile, Beer, Poo, Bug, Glasses, Meh} = SvgComponent()
  const router = useRoute()

  const [slider,setslider] = useState([
    {key:0,url:Smile,text:"Live Tv",color:"#EE9F06"},
    {key:1,url:Beer,text:"Movies",color:"#00B624"},
    {key:2,url:Poo,text:"PLaylist",color:"#4F4DFF"}
  ])

  const [lazyLoad,setLazyLoad] = useState(false)
  function onLoad() { setTimeout(() => { setLazyLoad(true) },500) }
    
          
  useEffect(async () => {
    getUserHomePage(await AsyncStorage.getItem('authenticated'))
    .then(async (response) => { await AsyncStorage.setItem("userID",response.user.id + "") })
  },[])
  

  useEffect(() => {
    if(route.params !== undefined) {
      let Params = JSON.parse(route.params)

      let [backColor] = Params.backColor.filter((val) => val.activeColor === true)
      let [imagine] = Params.icon.filter((val) => val.activeColor === true)
      let value = Params.value
      setslider([
        ...slider,
        {
          key:slider.length,
          url:(function() {
            let img = imagine.key === 0 
            ? Smile : imagine.key === 1 ? Beer : imagine.key === 2 ? Bug 
            : imagine.key === 3 ? Glasses : imagine.key === 4 ? Poo : 
            imagine.key === 5 ? Meh : null
            return img
          })(),
          text:value,
          color:backColor.color
        }
      ])
  }},[route.params])

  useEffect(() => {
    console.log("useEffect main page");
    BackHandler.addEventListener("hardwareBackPress", () => true);
    return () => console.log("log unmount main page")
  },[]);


  return (
      <View style={{flex: 1}}>
        <Lottery style={{zIndex:0,opacity:lazyLoad ?  0 : 1}}/>
        <ScrollView style={{zIndex:0,opacity:lazyLoad ?  1 : 0,position:"relative"}} >
          <ScrollView style={{height:Platform.OS === "ios" ? useSafeAreaFrame().height: vh(100)}}>
            <View style={styles.view2}>
              <Pressable style={styles.imgLogo}>
                  <Logo />
              </Pressable>
              <View style={styles.mainPageImg}>
                <Image source={require("../../assets/mainPageImg.png")}
                  onLoadEnd={() => onLoad(true)}
                />
              </View>
              <View style={styles.backgroundBottom}></View>
            </View>    
        
            <View style={styles.innerView3}>

              <Text style={styles.centerModeText}>Welcome to our App</Text>
              <View style={styles.form}>
                <View style={styles.imgInput}>
                  <Search/>
                </View>
                <Formik initialValues={{name:"", place:"Search.."}} onSubmit={(value,action) => {
                  action.resetForm();
                }}>
                  {(props) => ( 
                    <View>
                      <TextInput 
                        style={styles.input}
                        value={props.values.name}
                        placeholder={props.values.place}
                        onChangeText={props.handleChange("name")}
                      />
                      {/* <Button title="submit" 
                        onPress={props.handleSubmit}
                      /> */}
                    </View>
                  )}
                </Formik>
              </View>

              <View style={styles.slider}>
                <FlatList horizontal data={slider} renderItem={({item}) => {
                  return <TouchableOpacity onPress={() => navigation.navigate("LiveTv")} 
                    style={[styles.childSlider,{backgroundColor: item.color,}]}>
                    {(() => {
                      const CustomTag = item.url;
                      return <CustomTag/>
                    })()}
                    <Text style={styles.textSlider}>{item.text}</Text>
                  </TouchableOpacity>
                  }}
                  showsHorizontalScrollIndicator={false} 
                />
              </View>

              <View style={styles.multiPlay}>
                <TouchableOpacity  style={styles.multy} onPress={() => Alert.alert("do you want exit page","Confirm",[
                    {text:"yes", onPress:async () => {
                      postLogAuth(await AsyncStorage.getItem('authenticated'),navigation)
                    }},
                    {text:"cancel",onPress:() => null},
                  ])}>
                  <View style={styles.miniMulty}>
                    <Kubic/>
                  </View>
                  <Text style={{marginLeft:48,fontSize: 20}}>Log Auth</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.multy} onPress={() => navigation.navigate("User")}>
                  <View style={[styles.miniMulty,{backgroundColor: "#0085FF"}]}>
                    <CogWhite/>
                  </View>
                  <Text style={{marginLeft:48,fontSize: 20}}>Settings</Text>
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
          <Menyu navigation={navigation} backColor={"state1"}/>
        </ScrollView>
      </View>

  );
};


