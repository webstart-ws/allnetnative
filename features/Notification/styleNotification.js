import {StyleSheet} from "react-native"

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:"rgba(234, 234, 255, 1)"
    },
    imgLogo:{
        marginTop:Platform.OS === "ios" ? 23 : 43,
        marginLeft:15
    },
    contentText:{
        fontSize:25,
    },
    titleUserView:{
        marginTop:51,
        width:"100%",
        flexDirection: 'row',
        justifyContent:'space-between',
        alignItems: 'flex-end',
        paddingHorizontal: 15,
    },
    playlistUser:{
        width:"100%",
        borderRadius:20,
        marginTop:25,
        paddingHorizontal:15,
        marginBottom:78

    },
    childPlaylistUser:{
        position:"relative",
        width:"100%",
        padding: 13,
        borderRadius: 20,
        flexDirection:"row",
        alignItems:"center",
        marginBottom:10,
        backgroundColor:"white",
    },
    imgContent:{
        backgroundColor:"#EEBA06",
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width:77,
        height:77
    },
    textDiv:{
        marginLeft:18,
        position:"relative"
    },
    img:{
        width:30,
        height:30,
        position:"absolute",
        right:18,
        top:41,
        alignItems:"center"
    },
    descriptionChild:{
        flex:1,
        padding:20,
        backgroundColor:"rgba(234, 234, 255, 1)"
    },
    textDescr:{
        minHeight:300,
        fontSize: 30,
        backgroundColor:"white",
        borderRadius: 20,
        textAlign:"left",
        padding:10
    },
    doNotData:{
        flex:1,
        fontSize:50,
        color:"red",
        textAlign:"center",
        justifyContent:"center",
        marginTop:30
    }
})