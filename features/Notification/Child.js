import React from "react";
import {View,Text, Platform}from "react-native";
import { styles } from "../Notification/styleNotification";

export function ChildNotification({route}) {
    return (
        <View style={styles.descriptionChild}>
            <Text style={[styles.textDescr,{marginTop:Platform.OS === "ios" ? 0 : 25}]}>{route.params.description}</Text>
        </View>
    )
}