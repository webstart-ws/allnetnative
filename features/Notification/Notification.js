import React, { useEffect, useState } from "react";
import { styles } from "../Notification/styleNotification";
import { SafeAreaView, Image, Text, View, TouchableWithoutFeedback, Alert, TouchableOpacity, Pressable, BackHandler, ScrollView } from "react-native";
import { Menyu } from '../menyu/menyu';
import { Lottery } from "../Lottery/Lottery";
import { SvgComponent } from "../Svg";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { NotificationFetch } from "../AllRequest/getRequest";
import { deleteMessagePost } from "../AllRequest/postRequest";




export function Notification({navigation}) {
  const  { Logo, Bell, Deletedots, Dots } = SvgComponent()

    const [playlistData,setPlaylistData] = useState([
      // {id:1,closeIcon:<Deletedots/>,imgRef:<Bell/>,color:"#4F4DFF",text:"Lorem Ipsum is simply dummy text of the",createdTime:"24 mi ago"},
    ]) 

    useEffect(() => {
      BackHandler.addEventListener("hardwareBackPress", () => navigation.goBack());
      console.log("mount notification")
      return () =>  {
          console.log("unmount notification")
          BackHandler.addEventListener("hardwareBackPress", () => true);
      };
    }, []);

    useEffect(async () => {
      const  userID = await AsyncStorage.getItem('userID')
      const  token = await AsyncStorage.getItem('authenticated')

      NotificationFetch(token,userID).then((resp) => {
        const updateplaylistData = resp.notifications.map((val) => { 
          return {id:val.id,closeIcon:<Deletedots/>,imgRef:<Bell/>,color:"#00B624",text:val.title,createdTime:val.created_at,description:val.description}
        })
        setPlaylistData( updateplaylistData )
      })
    },[])
    console.log("notification",playlistData.length);



    return (
      <>
        <SafeAreaView style={[styles.container]}>
          <Pressable style={styles.imgLogo}>
              <Logo />
          </Pressable>
            
          <View style={styles.titleUserView}>
            <Text style={styles.contentText}>Notification</Text>
            <TouchableWithoutFeedback onPress={() => Alert.alert("what is want to use","mainMessige",[
                {text:"yes", onPress:() => null},
                {text:"cancel",onPress:() => null},
            ])}>
              <View style={{width:30,height:20}}>
                <Dots/>
              </View>
            </TouchableWithoutFeedback>
          </View>

          <ScrollView style={styles.playlistUser}>
          
            {
            !!playlistData[0]  ? 
            playlistData.map((val,index) => {
              return  (
                <View key={val.id}>
                  <TouchableOpacity style={styles.childPlaylistUser} onPress={() => navigation.navigate("ChildNotification",val)}>
                    <View style={[styles.imgContent,{backgroundColor:val.color}]}>
                      {val.imgRef}
                    </View>

                    <View style={styles.textDiv}>
                      <View>
                        <Text style={{fontSize:16,fontWeight:"bold",width:212}}>{val.text}</Text>
                        <Text style={{fontSize:14,opacity:0.2,paddingTop:5}}>{val.createdTime}</Text>
                      </View>
                    </View>
                  </TouchableOpacity>

                  <TouchableWithoutFeedback onPress={() => {
                    Alert.alert("Delete","Do you want to delete this notification",[
                      {text:"Yes", onPress:async () => {
                        const  userID = await AsyncStorage.getItem('userID')
                        const  token = await AsyncStorage.getItem('authenticated')
                        deleteMessagePost(token,val.id)
                        setPlaylistData(playlistData.filter((val,i) => { return i !== index }))
                      }},
                      {text:"No",onPress:() => null},
                  ])}}>
                    <View style={styles.img}>
                      {val.closeIcon}
                    </View>
                  </TouchableWithoutFeedback>
                </View>
              )}) : playlistData.length === 0 ?
              <Text style={styles.doNotData}>There are'nt Notification</Text> :
              <Image style={ { width: 333 }} source={ require(`../../assets/homeLoading.gif`)} /> 
          }
          </ScrollView>
          <Menyu navigation={navigation} backColor={"state5"}/>
        </SafeAreaView>
      </>
    )
}