const server = "https://allnet.webstart.am/api"
import AsyncStorage from '@react-native-async-storage/async-storage';




export async function postSignIn({body}) {
    const response = await fetch(`${server}/login`,{
        mode: 'cors',
        method : "POST",
        headers : {'Content-Type' : 'application/json','Accept': 'application/json'},
        body : JSON.stringify(body)
        // credentials: "same-origin",
        // headers : {'Content-Type' : 'application/json','Accept': 'application/json', "XSRF-TOKEN" : token},
    })
    return response.json()
}

export async function postLogAuth(token,navigation) {
    await fetch(`${server}/logout`,{
        mode: 'cors',
        method : "POST",
        headers :{'Content-Type' : 'application/json','Accept' : 'application/json','Authorization': `Bearer ${token}`}
    }).then(async (resp) => { 
        await AsyncStorage.removeItem("authentication");
        await AsyncStorage.removeItem("userID");
        navigation.navigate("Home")
        console.log("log auth")
    });
}

export async function changeUserData({token,body,id}) {
    const response = await fetch(`${server}/user/${id}`,{
        mode: 'cors',
        method : "PUT",
        headers : {'Content-Type' : 'application/json','Accept' : 'application/json','Authorization' : `Bearer ${token}`},
        body:JSON.stringify(body)
    })
    return response.json()
}



export async function deleteMessagePost(token,id) {
    const response = await fetch(`${server}/notification/${id}`,{
        mode: 'cors',
        method : "DELETE",
        headers : {'Content-Type' : 'application/json','Accept' : 'application/json','Authorization' : `Bearer ${token}`},
        body:JSON.stringify(id)
    })
    return response.json()
}

export async function postRegister(body) {
    const response = await fetch(`${server}/register`,{
        mode: 'cors',
        method : "POST",
        headers :{'Content-Type' : 'application/json','Accept': 'application/json'},
        body : JSON.stringify(body)
    })
    return response.json()
}


