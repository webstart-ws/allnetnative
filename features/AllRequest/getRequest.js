const server = "https://allnet.webstart.am/api"

export async function getHome() {
    return await fetch(`${server}/home`).then((result) => result.json())
}

export async function getUserHomePage(token) {
    let configHeader = { mode: 'cors', method : "GET", headers : {'Content-Type' : 'application/json','Accept' : 'application/json','Authorization': `Bearer ${token}`} };
    const response = await fetch(`${server}/userPage`,configHeader).then((result) => result.json())
    return response
}



export async function NotificationFetch(token,id) {
    let configHeader = { 
        mode: 'cors', method : "GET", headers : {'Content-Type' : 'application/json','Accept' : 'application/json','Authorization': `Bearer ${token}`}
    };
    const response = await fetch(`${server}/notification/${id}`,configHeader).then((result) => result.json())
    return response
}
