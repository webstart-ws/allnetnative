import LottieView from "lottie-react-native"
import React from "react"


export function Lottery() {
    return (
        <LottieView source={require("../../assets/fonts/66433-loader.json")} autoPlay loop/>
    )
}

