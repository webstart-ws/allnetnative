import { useEffect, useState } from "react"
import { Dimensions } from "react-native"

    
export function Orientation() {
    let [screenInfo,setScreenInfo] = useState(Dimensions.get("screen"))
    // console.log(screenInfo)


    useEffect(() => {
        const onchange = (result) => {
            setScreenInfo(result.screen)
        }

        Dimensions.addEventListener("change",onchange)

        return () => Dimensions.removeEventListener("change",onchange)

    },[])

    return {
        ...screenInfo,
        isPortrait: screenInfo.height > screenInfo.width
    }
}
