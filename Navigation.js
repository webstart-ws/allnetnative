import React, { useEffect, useState } from "react"
import { MainPages } from "./features/mainPages/MainPages"
import { Notification } from "./features/Notification/Notification";
import { LiveTv } from "./features/liveTV/liveTv";
import { ListUser } from "./features/ListUser/listUser";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { AddPlaylist } from "./features/addPlaylist/addPlaylist";
import { Channel } from "./features/liveTV/Channel";
import { Playlist } from "./features/mainPages/Playlist";
import { PlaylistMini } from "./features/ListUser/playlistMini";
import { MultyScreen } from "./features/multiScreen/multiScreen";
import { Home } from "./features/homePage/home";
import { SignIn } from "./features/signIn/signIn";
import { Register } from "./features/register/register"
import { User } from "./features/User/user";
import LottieView from "lottie-react-native";
import { Lottery } from "./features/Lottery/Lottery";
import { FindByEmail } from "./features/forgetPassword/forgetPassword";
import { CreatePassword } from "./features/forgetPassword/creAtePassword";
import { ChildNotification } from "./features/Notification/Child";
import { Dimensions, Image, Text, View } from 'react-native';

const Stack = createStackNavigator();



export default function Navigate() {

    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Home" component={Home} options={{headerShown:false}} />
                <Stack.Screen
                    name="Register"
                    component={Register}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="SignIn"
                    component={SignIn}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="MainPages"
                    component={MainPages}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="Notification"
                    component={Notification}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="LiveTv"
                    component={LiveTv}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="ListUser"
                    component={ListUser}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="AddPlaylist"
                    component={AddPlaylist}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="Channel"
                    component={Channel}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="Playlist"
                    component={Playlist}
                    options={{headerShown:false}}
                />
                <Stack.Screen 
                    name="PlaylistMini"
                    component={PlaylistMini}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="MultyScreen"
                    component={MultyScreen}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="User"
                    component={User}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="FindByEmail"
                    component={FindByEmail}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="CreatePassword"
                    component={CreatePassword}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="ChildNotification"
                    component={ChildNotification}
                    options={{headerShown:false}}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
    
}