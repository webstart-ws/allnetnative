import { StatusBar } from 'expo-status-bar';
import React, { Suspense,lazy, useEffect, useState } from 'react';
import * as Font from "expo-font"

import { StyleSheet, SafeAreaView , View, Text,BackHandler} from 'react-native';
import AppLoading from 'expo-app-loading';
import Navigate from './Navigation';
import { vw, vh, vmin, vmax } from 'react-native-expo-viewport-units';
import LottieView from "lottie-react-native"
import AsyncStorage from '@react-native-async-storage/async-storage'
import {en,am} from "./localization"
// import * as Localization from 'expo-localization'



const fonts = () => Font.loadAsync({
  "mt-bold" : require("./assets/fonts/Montserrat-Bold.ttf"),
  "mt-light" : require("./assets/fonts/Montserrat-Light.ttf")
})




export default function App() {
  console.log("app");
  AsyncStorage.getItem('authenticated').then((resp) => {
    console.log(resp,"appContainer");
  });


  const [font,setFont] = useState(false)
  if(font) {
    return (
      <SafeAreaView style={styles.container}>
        <Navigate/> 
        <StatusBar styles="auto"/>
      </SafeAreaView>
    );
  } else {
    return (
      <AppLoading startAsync={fonts} onFinish={() => setFont(true)} onError={console.warn}/>
    )
  }
};


export const styles = StyleSheet.create({
  container:{
    flex:1,
    fontFamily:"mt-light",
  },
})